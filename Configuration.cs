namespace Laundry.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Laundry.Model;

    internal sealed class Configuration : DbMigrationsConfiguration<Laundry.DAL.LaundryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Laundry.DAL.LaundryContext";
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Laundry.DAL.LaundryContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

         /*   context.Roles.AddOrUpdate(
           p => p.RoleName,
           new Role { RoleName = "Administrator", Status = 1, CreatedBy = "System", CreatedOn = DateTime.Today, ModifiedBy = "System", ModifiedOn = DateTime.Today },
           new Role { RoleName = "User", Status = 1, CreatedBy = "System", CreatedOn = DateTime.Today, ModifiedBy = "System", ModifiedOn = DateTime.Today }
           );*/

          /*  LaundryMan LaundryManObj = new LaundryMan();
            LaundryManObj.Surname = "Administrator";
            LaundryManObj.Othername = "Administrator";
            LaundryManObj.Sex = "Male";
            LaundryManObj.PhoneNumber = "07053900429";
            LaundryManObj.Address = "TBA";
            LaundryManObj.Username = "admin";
            LaundryManObj.Password = "admin";
            LaundryManObj.RoleID = 1;
            LaundryManObj.Status = 1;
            LaundryManObj.Flag = "A";
            LaundryManObj.Keydate = DateTime.Now;
            context.LaundryMans.AddOrUpdate(c => c.Username, LaundryManObj);
            */

            /*CompanyDetail CompanyDetailObj = new CompanyDetail();
            CompanyDetailObj.Company_Code = "Test";
            CompanyDetailObj.Company_Name = "Test Laundry";
            CompanyDetailObj.Company_ShortName = "Test Laundry";
            CompanyDetailObj.Company_Address = "Lagos";
            CompanyDetailObj.Company_Phone1 = "07053900429";
            CompanyDetailObj.Company_Phone2 = "07053900429";
            CompanyDetailObj.Company_Email = "test@yahoo.com";
            CompanyDetailObj.Company_Username = "admin";
            CompanyDetailObj.Company_Password = "admin";
            CompanyDetailObj.Company_Flag = "A";
            CompanyDetailObj.Company_KeyDate = DateTime.Now;
            context.CompanyDetails.AddOrUpdate(c => c.Company_Username, CompanyDetailObj);*/


            EmailSetting EmailSettingObj = new EmailSetting();
            EmailSettingObj.SmtpServer = "mail.sharonklean.com";
            EmailSettingObj.PortNo = 25;
            EmailSettingObj.FromEmail = "test@sharonklean.com";
            EmailSettingObj.Password = "yakdseeker@1";
            EmailSettingObj.UserId = "admin";
            EmailSettingObj.Flag = "A";
            EmailSettingObj.KeyDate = DateTime.Now;
            context.EmailSettings.AddOrUpdate(c => c.UserId, EmailSettingObj);

            //Starch Level

            /*  context.Code.AddOrUpdate(
                p => p.Codes_Desc,
                new Codes { Codes_Type = "Cod003", Codes_Desc = "Light", Codes_Val = "Li", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "admin" },
                new Codes { Codes_Type = "Cod003", Codes_Desc = "Medium", Codes_Val = "Me", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "admin" },
                new Codes { Codes_Type = "Cod003", Codes_Desc = "Heavy", Codes_Val = "He", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "admin" }
                );
                */

            //Pressing Options
            /*
            context.Code.AddOrUpdate(
              p => p.Codes_Desc,
              new Codes { Codes_Type = "Cod004", Codes_Desc = "Dry", Codes_Val = "D", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "admin" },
              new Codes { Codes_Type = "Cod004", Codes_Desc = "Steam", Codes_Val = "S", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "admin" }
              );*/

            //Gender Type
             context.Code.AddOrUpdate(
             p => p.Codes_Desc,
             new Codes { Codes_Type = "GenderType", Codes_Desc = "Men", Codes_Val = "1", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "admin" },
             new Codes { Codes_Type = "GenderType", Codes_Desc = "Ladies", Codes_Val = "2", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "admin" }
             );

            //ServiceType
            context.Code.AddOrUpdate(
            p => p.Codes_Desc,
            new Codes { Codes_Type = "ServiceType", Codes_Desc = "Laundry", Codes_Val = "1", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "admin" },
            new Codes { Codes_Type = "ServiceType", Codes_Desc = "Dry-Cleaning", Codes_Val = "2", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "admin" }
            );

        }
    }
}
