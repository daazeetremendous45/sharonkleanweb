﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Laundry.BLL;
using Laundry.Model;
using System.Data;

namespace Laundry.Job
{
    class Program
    {
        static void Main(string[] args)
        {
            RunSchedule();
        }

        public static void RunSchedule()
        {
            EmailTransaction EmailTransactionObj = new EmailTransaction();
            DataRow dr = null;
            try
            {
                var UnSentEmail = JobEmailTransactionBs.GetFailedEmailDueToInternet();

                if (UnSentEmail.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < UnSentEmail.Tables[0].Rows.Count; i++)
                    {
                        dr = UnSentEmail.Tables[0].Rows[i];
                        EmailTransactionObj.MessageID = dr["MessageID"].ToString();
                        EmailTransactionObj.EmailAddress = dr["EmailAddress"].ToString();
                        EmailTransactionObj.EmailSubject = dr["EmailSubject"].ToString();
                        EmailTransactionObj.EmailBody = dr["EmailBody"].ToString();
                        EmailTransactionObj.EmailSignature = dr["EmailSignature"].ToString();
                        JobEmailTransactionBs.SendUnSentEmailByJob(EmailTransactionObj);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }
        }
    }
}
