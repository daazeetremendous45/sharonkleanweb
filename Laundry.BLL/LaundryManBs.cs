﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.Model;
using Laundry.DAL;

namespace Laundry.BLL
{
  public class LaundryManBs
    {
        private LaundryManDA NewLaundryManDA = new LaundryManDA();
        private HashHelper hashHelper = new HashHelper();
        public IEnumerable<LaundryMan> ListAll()
        {
            return NewLaundryManDA.ListAll();
        }


        public IEnumerable<LaundryMan> ListAllByStatus(int status)
        {
            return NewLaundryManDA.ListAllByStatus(status);
        }

        public string UpdateStatus(string username, int status)
        {
            return NewLaundryManDA.UpdateStatus(username,status);
        }
        public LaundryMan GetById(int id)
        {
            return NewLaundryManDA.GetById(id);
        }

        public LaundryMan GetByUsername(string username)
        {
            return NewLaundryManDA.GetByUsername(username);
        }

        public string VerifyUsername(string username)
        {
            return NewLaundryManDA.VerifyUsername(username);
        }
        public void Insert(LaundryMan LaundryManBsObj)
        {
            LaundryManBsObj.Password = HashHelper.Encrypty(HashHelper.HashPassword(LaundryManBsObj.Password));
            NewLaundryManDA.Insert(LaundryManBsObj);
        }

        public void Update(LaundryMan LaundryManBsObj)
        {
            var LaundryManExist = NewLaundryManDA.GetByUsername(LaundryManBsObj.Username);
            LaundryManExist.Password = LaundryManBsObj.Password;
            LaundryManExist.Flag = LaundryManBsObj.Flag;
            NewLaundryManDA.Update(LaundryManExist);
        }

        public void UpdateUserRoleStatus(LaundryMan LaundryManBsObj)
        {
            var LaundryManExist = NewLaundryManDA.GetById(LaundryManBsObj.LaundryManID);
            LaundryManExist.Status = LaundryManBsObj.Status;
            LaundryManExist.RoleID = LaundryManBsObj.RoleID;
            LaundryManExist.BranchID = LaundryManBsObj.BranchID;
            LaundryManExist.Flag = LaundryManBsObj.Flag;
            NewLaundryManDA.Update(LaundryManExist);
        }

        //public string Login(string username, string password)
        //{
        //  return NewLaundryManDA.Login(username,password);
        //}

        public string LoginNew(string username, string password)
        {
            string result = "";
            var search = NewLaundryManDA.LoginNew(username);

            if (search == null)
            {
                result = "Username does not exist";
            }
            else if (HashHelper.Decrypty(search.Password) != HashHelper.HashPassword(password))
            {
                result = "Invalid password";
            }
            else if (search.Status == 0 && search.Flag == "A")//A fresh registration
            {
                result = "User has not been activated";
            }
            else if (search.Status == 0 && search.Flag == "C")//A fresh registration
            {
                result = "User has been deactivated";
            }

            return result;

        }

    }
    }
