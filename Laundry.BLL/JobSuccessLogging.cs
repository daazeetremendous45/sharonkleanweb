﻿using System;
using System.IO;

namespace Laundry.BLL
{
    public static class JobSuccessLogging
    {
        public static void SendSuccessStatusToText()
        {
            var line = Environment.NewLine + Environment.NewLine;

            try
            {
                string filepath = Path.GetFullPath(@"E:\CSharpMVCProject\MVCApps\laundry1.0\LaundrySoln\Laundry.Job\log\");

                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);

                }
                string filename = DateTime.Now.ToString("MM_dd_yyyy_HH_mm") + "_log.txt";

                string fullpath = filepath + filename;
                if (!File.Exists(fullpath))
                {
                    File.Create(fullpath).Dispose();
                }

                using (StreamWriter sw = File.AppendText(fullpath))
                {
                    string error = "Job run successfully";
                    sw.WriteLine("-----------Run on " + " " + DateTime.Now.ToString() + "-----------------");
                    sw.WriteLine("-------------------------------------------------------------------------------------");
                    sw.WriteLine(error);
                    sw.WriteLine("--------------------------------*End*------------------------------------------");
                    sw.Flush();
                    sw.Close();
                }

            }
            catch (Exception e)
            {
                ExceptionLogging.SendErrorToText(e);
            }
        }

    }
}