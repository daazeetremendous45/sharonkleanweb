﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.DAL;
using Laundry.Model;
using System.Net.Mail;
using System.Net;

namespace Laundry.BLL
{
    public class EmailTransactionBs
    {
        private EmailTransactionDA NewEmailTransactionDA = new EmailTransactionDA();

        EmailSetting EmailSettingsObj = new EmailSetting();
        EmailSettingBs NewEmailSettingBs = new EmailSettingBs();

        MailMessage mail = new MailMessage();

        public LaundryContext context = new LaundryContext();
        private string message;
        public IEnumerable<EmailTransaction> ListAll()
        {
            return NewEmailTransactionDA.ListAll();
        }

        public IEnumerable<EmailTransaction> GetEmailTransactionByStatus(string status)
        {
            return NewEmailTransactionDA.GetEmailTransactionByStatus(status);
        }

        public IEnumerable<EmailTransaction> GetEmailTransactionByTransNo(string transno)
        {
            return NewEmailTransactionDA.GetEmailTransactionByTransNo(transno);
        }
        public EmailTransaction GetById(string id)
        {
            return NewEmailTransactionDA.GetById(id);
        }
        public void Insert(EmailTransaction EmailTransactionObj)
        {
            NewEmailTransactionDA.Insert(EmailTransactionObj);
        }

        public void Update(EmailTransaction EmailTransactionObj)
        {
            NewEmailTransactionDA.Update(EmailTransactionObj);
        }

        public void Delete(string id)
        {
            NewEmailTransactionDA.Delete(id);
        }

        public Boolean IsConnectionAvailable()
        {
            //Returns True if connection is available
            //Replace www.yoursite.com with a site that
            //is guaranteed to be online - perhaps your
            //corporate site, or microsoft.com
            var objUrl = new System.Uri("http://www.gmail.com/");
            //Setup WebRequest
            WebRequest objWebReq;
            objWebReq = WebRequest.Create(objUrl);
            WebResponse objResp;
            try
            {
                //Attempt to get response and return True
                objResp = objWebReq.GetResponse();
                objResp.Close();
                objWebReq = null;
                return true;
            }
            catch (Exception ex)
            {
                //Error, exit and return False
                //objResp.Close()
                objWebReq = null;
                return false;
            }

            //Here() 's how you might use this function in your application:

        }

        private void insertEmail()
        {

        }

        public string SendNewTransactionMail(EmailTransaction EmailTransactionObj)
        {
            var EmailSettingResult = NewEmailSettingBs.GetEmailSetting();
            string FromEmail = EmailSettingResult.FromEmail;
            string Password = EmailSettingResult.Password;
            int PortNo = EmailSettingResult.PortNo;
            string Host = EmailSettingResult.SmtpServer;

            string MailStatus;

            mail = new MailMessage();
            mail.From = new MailAddress(FromEmail);
            mail.To.Add(EmailTransactionObj.EmailAddress);
            
            mail.Subject = "Test Laundry: Your Membership Password";
            mail.Body = EmailTransactionObj.EmailBody;
            mail.IsBodyHtml = true;
            SmtpClient SmtpServer = new SmtpClient();
            SmtpServer.Port = PortNo;
            SmtpServer.Host = Host;
            //SmtpServer.EnableSsl = true; //This was commented after setting up for sharonklean. Working for gmail before
            NetworkCredential NetworkCred = new NetworkCredential(FromEmail, Password);
            SmtpServer.UseDefaultCredentials = true;
            SmtpServer.Credentials = NetworkCred;

            if (IsConnectionAvailable() == true)
            {
                try
                {
                    SmtpServer.Send(mail);
                    MailStatus = "Delivered";
                    EmailTransactionObj.MessageStatus = MailStatus;
                    Insert(EmailTransactionObj);
                 
                }

                catch (Exception ex)
                {
                    MailStatus = ex.Message;
                    EmailTransactionObj.MessageStatus = ex.ToString();
                    Insert(EmailTransactionObj);
                }
            }
            else
            {
                MailStatus = "Internet connection not available";
                EmailTransactionObj.MessageStatus = MailStatus;
                Insert(EmailTransactionObj);
            }
            return MailStatus;
        }

       

    }
}
