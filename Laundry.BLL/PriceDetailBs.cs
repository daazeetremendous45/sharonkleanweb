﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.DAL;
using Laundry.Model;
using Laundry.Model.Custom;
namespace Laundry.BLL
{
    public class PriceDetailBs
    {
        private PriceDetailDA priceDetailDA = new PriceDetailDA();

        public IEnumerable<PriceDetailDescriptions> ListAll()
        {
            return priceDetailDA.ListAll();
        }

        public PriceDetail GetById(int id)
        {
            return priceDetailDA.GetById(id);
        }

        public void Insert(PriceDetail garmentObj)
        {
            priceDetailDA.Insert(garmentObj);
        }

        public void Update(PriceDetail garmentObj)
        {
            var result = GetById(garmentObj.PriceDetailID);
            result.Amount = garmentObj.Amount;
            priceDetailDA.Update(result);
        }

        public void Delete(int id)
        {
            priceDetailDA.Delete(id);
        }

        public  PriceDetail GetPriceByGarmentCleaningCatServiceType(int garmentId, string cleaningCategory, string serviceType)
        {
        return  priceDetailDA.GetPriceByGarmentCleaningCatServiceType(garmentId, cleaningCategory, serviceType);
        }

        public PriceDetail GetPriceByGarmentCleaningCat(int garmentId, string cleaningCategory)
        {
            return priceDetailDA.GetPriceByGarmentCleaningCat(garmentId, cleaningCategory);
        }
    }
}
