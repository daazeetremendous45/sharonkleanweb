﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.Model;
using Laundry.DAL;

namespace Laundry.BLL
{
  public  class BranchBs
    {
        private BranchDA NewBranchDA = new BranchDA();

        public IEnumerable<Branch> ListAll()
        {
            return NewBranchDA.ListAll();
        }
        
        
        public Branch GetById(int id)
        {
            return NewBranchDA.GetById(id);
        }

        public Branch GetByName(string name)
        {
            return NewBranchDA.GetByName(name);
        }
        public void Insert(Branch BranchObj)
        {
            NewBranchDA.Insert(BranchObj);
        }

        public void Update(Branch BranchObj)
        {
            var BranchExist= NewBranchDA.GetById(BranchObj.BranchID);
            BranchExist.Address = BranchObj.Address;
            BranchExist.PhoneNumber1 = BranchObj.PhoneNumber1;
            BranchExist.PhoneNumber2 = BranchObj.PhoneNumber2;
            BranchExist.ModifiedBy = BranchObj.ModifiedBy;
            BranchExist.ModifiedOn = BranchObj.ModifiedOn;
            NewBranchDA.Update(BranchExist);
        }
    }
}

