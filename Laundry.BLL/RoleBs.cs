﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.DAL;
using Laundry.Model;

namespace Laundry.BLL
{
    public class RoleBs
    {
        private RoleDA NewRoleDA = new RoleDA();
        public IEnumerable<Role> GetAllRoles()
        {
            return NewRoleDA.GetAllRoles();
        }
        public Role GetRoleIdByName(string roleName)
        {
            return NewRoleDA.GetRoleIdByName(roleName);
        }
    }
}
