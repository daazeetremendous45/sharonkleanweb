﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.DAL;
using Laundry.Model;
using System.Net.Mail;
using System.Net;
using System.Data;

namespace Laundry.BLL
{
    public class JobEmailTransactionBs
    {

        public static DataSet GetFailedEmailDueToInternet()
        {
            return JobEmailTransactionDA.GetFailedEmailDueToInternet();
        }

        //public static void UpdateFailedEmailStatus(string id)
        //{
        //    JobEmailTransactionBs.UpdateFailedEmailStatus(id);
        //}

        public static void SendUnSentEmailByJob(EmailTransaction EmailTransactionObj)
        {
            var EmailSettingsDs = new DataSet();
            string FromEmail;
            string Password;
            int PortNo;
            string Host;
            MailMessage mail = new MailMessage();

            EmailSettingsDs = JobEmailSettingDA.GetEmailSetting();
            if (EmailSettingsDs.Tables[0].Rows.Count > 0)
            {
                DataRow EmailSettingsDataRow = EmailSettingsDs.Tables[0].Rows[0];
                FromEmail = EmailSettingsDataRow["FromEmail"].ToString();
                Password = EmailSettingsDataRow["Password"].ToString();
                PortNo = Convert.ToInt32(EmailSettingsDataRow["PortNo"]);
                Host = EmailSettingsDataRow["SmtpServer"].ToString();

                mail = new MailMessage();
                mail.From = new MailAddress(FromEmail);
                mail.To.Add(EmailTransactionObj.EmailAddress);

                mail.Subject = "Test Laundry: Your Membership Password";
                mail.Body = EmailTransactionObj.EmailBody;
                mail.IsBodyHtml = true;
                SmtpClient SmtpServer = new SmtpClient();
                SmtpServer.Port = PortNo;
                SmtpServer.Host = Host;
                SmtpServer.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(FromEmail, Password);
                SmtpServer.UseDefaultCredentials = true;
                SmtpServer.Credentials = NetworkCred;


                if (IsConnectionAvailable() == true)
                {
                    try
                    {
                        // SmtpServer.Send(mail);
                        JobEmailTransactionDA.UpdateFailedEmailStatus(EmailTransactionObj.MessageID);
                        JobSuccessLogging.SendSuccessStatusToText();
                    }

                    catch (Exception ex)
                    {
                        ExceptionLogging.SendErrorToText(ex);
                    }
                }
            }
        }

        public static bool IsConnectionAvailable()
        {

            var objUrl = new System.Uri("http://www.gmail.com/");
            WebRequest objWebReq;
            objWebReq = WebRequest.Create(objUrl);
            WebResponse objResp;
            try
            {
                //Attempt to get response and return True
                objResp = objWebReq.GetResponse();
                objResp.Close();
                objWebReq = null;
                return true;
            }
            catch (Exception ex)
            {

                objWebReq = null;
                return false;
            }
        }
    }
}
