﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.DAL;
using Laundry.Model;

namespace Laundry.BLL
{
   public  class CustomerBs
    {

        private CustomerDA NewCustomerDA = new CustomerDA();
        public IEnumerable<Customer> GetAllCustomer()
        {
            return NewCustomerDA.GetAllCustomer();
        }

        public Customer GetById(string id)
        {
            return NewCustomerDA.GetById(id);
        }
        public void Insert(Customer CustomerDAObj)
        {
            NewCustomerDA.Insert(CustomerDAObj);
        }

        public void Update(Customer CustomerDAObj)
        {
           var CustomerObjExist = NewCustomerDA.GetByPhoneNumber(CustomerDAObj.CustomerPhone);
            if (CustomerObjExist != null)
            {
                CustomerObjExist.Surname = CustomerDAObj.Surname;
                CustomerObjExist.Othername = CustomerDAObj.Othername;
                CustomerObjExist.Address = CustomerDAObj.Address;
                CustomerObjExist.EmailAddress = CustomerDAObj.EmailAddress;
                CustomerObjExist.UserId = CustomerDAObj.UserId;
                CustomerObjExist.Keydate = CustomerDAObj.Keydate;
                CustomerObjExist.Flag = CustomerDAObj.Flag;
                NewCustomerDA.Update(CustomerObjExist);
            }
        }

        public void Delete(string id)
        {
            NewCustomerDA.Delete(id);
        }

        public Customer GetByPhoneNumber(string PhoneNumber)
        {
            return NewCustomerDA.GetByPhoneNumber(PhoneNumber);
        }

        public Customer GetByEmail(string Email)
        {
            return NewCustomerDA.GetByEmail(Email);
        }

        public string GetCustomerID(string PhoneNumber)
        {
           return GetByPhoneNumber(PhoneNumber).CustomerId;
        }

        public IEnumerable<Customer> SearchCustomerByName(string name)
        {

            return NewCustomerDA.SearchCustomerByName(name);
        }

        public double CustomerSearchTotalPages(int count=0)
        {
            double totalpages = 1;
            if(count != 0)
            {
                totalpages= Math.Ceiling(count / 10.0);
            }
            return totalpages;
        }

        public Customer GetLastRegisteredCustomer()
        {

            return NewCustomerDA.GetLastRegisteredCustomer();
        }
    }
}
