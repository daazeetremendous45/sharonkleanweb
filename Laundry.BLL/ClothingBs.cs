﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.DAL;
using Laundry.Model;
using Laundry.Model.Custom;
namespace Laundry.BLL
{
    public class ClothingBs
    {
        private ClothingDA NewClothingDA = new ClothingDA();

        public IEnumerable<ClothingDetail> ListAll()
        {
            return NewClothingDA.ListAll();
        }

        public Clothing GetById(int id)
        {
            return NewClothingDA.GetById(id);
        }

        //public Clothing GetByClothValue(string cloth_val)
        //{
        //    //Get Code Description By Code Value
        //    return NewClothingDA.GetByClothValue(cloth_val);
        //}

        public Clothing GetByClothDesc(string cloth_desc)
        {
            //Get Code Description By Code Value
            return NewClothingDA.GetByClothDesc(cloth_desc);
        }

        //public Clothing ClothDescripton(string cloth_id, string cloth_desc)
        //{
        //    return NewClothingDA.ClothDescription(cloth_id, cloth_desc);
        //}
        public void Insert(Clothing ClothingBsObj)
        {
            NewClothingDA.Insert(ClothingBsObj);
        }

        public void Update(Clothing ClothingBsObj)
        {
            var ClothingExist = GetById(ClothingBsObj.ClothingID);
            ClothingExist.ClothDesc = ClothingBsObj.ClothDesc;
            ClothingExist.Amount = ClothingBsObj.Amount;
            ClothingExist.Flag = ClothingBsObj.Flag;
            NewClothingDA.Update(ClothingExist);
        }

        public async Task<Clothing> GetByGarmentGenderServiceTypeAsync(string clothDesc, string gender, string serviceType)
        {
            return await NewClothingDA.GetByGarmentGenderServiceTypeAsync(clothDesc, gender, serviceType);
        }
    }
}
