﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.DAL;
using Laundry.Model;
using Laundry.Model.Custom;
namespace Laundry.BLL
{
    public class GarmentBs
    {
        private GarmentDA garmentDA = new GarmentDA();

        public IEnumerable<Garment> ListAll()
        {
            return garmentDA.ListAll();
        }

        public Garment GetById(int id)
        {
            return garmentDA.GetById(id);
        }

        public Garment GetByName(string name)
        {
            return garmentDA.GetByName(name);
        }

        public void Insert(Garment garmentObj)
        {
            garmentDA.Insert(garmentObj);
        }

        public void Update(Garment garmentObj)
        {
            garmentDA.Update(garmentObj);
        }

        public void Delete(int id)
        {
            garmentDA.Delete(id);
        }
    }
}
