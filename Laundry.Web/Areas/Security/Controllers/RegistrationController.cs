﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Laundry.Model;
using Laundry.BLL;
using System.IO;

namespace Laundry.Web.Areas.Security.Controllers
{
    public class RegistrationController : Controller
    {
        // GET: Security/Registration
        LaundryManBs NewLaundryManBs = new LaundryManBs();
        LaundryMan LaundryManObj = new LaundryMan();

        CompanyDetailBs NewCompanyDetailBs = new CompanyDetailBs();
        CompanyDetail CompanyDetailObj = new CompanyDetail();
        CompanyLogoBs NewCompanyLogoBs = new CompanyLogoBs();
        CompanyLogo CompanyLogoObj = new CompanyLogo();
        CompanyDetailBs companyDetailBs = new CompanyDetailBs();
        // GET: Security/Registration
        CustomerBs NewCustomerBs = new CustomerBs();
        RoleBs roleBs = new RoleBs();
        Model.Customer CustomerObj = new Model.Customer();
        string message;


        HttpPostedFileBase postedImage;
        private static byte[] byteImageData;
        public ActionResult ListLaundryMan(string status)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            //if (status == null)
            //{
            //    status = "P";
            //}
            //ViewBag.status = status;
            //if (status == "L")
            //{
            //    return View(NewLaundryManBs.ListAll());
            //}

            //return View(NewLaundryManBs.ListAllByStatus(status));
            return View(NewLaundryManBs.ListAll());
        }

        // GET: Security/Registration/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Security/Registration/Create
        public ActionResult LaundryManRegistration(string ValidateReg = "")
        {
            if (ValidateReg != "Y")
            {
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            return View();
        }

        // POST: Security/Registration/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LaundryManRegistration(FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                LaundryManObj.Surname = collection["Surname"];
                LaundryManObj.Othername = collection["Othername"];
                LaundryManObj.Sex = collection["Sex"];
                LaundryManObj.PhoneNumber = collection["PhoneNumber"];
                LaundryManObj.Address = collection["Address"];
                LaundryManObj.Username = collection["Username"];
                LaundryManObj.Password = collection["Password"];
                LaundryManObj.Company_Id = companyDetailBs.GetCompanyDetail().Company_Id;

                if (collection["Password"] == collection["ConfirmPassword"])
                {
                    var result = NewLaundryManBs.GetByUsername(LaundryManObj.Username);

                    if (result != null)
                    {
                        ViewData["Message"] = "Username already exist";
                        return View(LaundryManObj);
                    }
                    LaundryManObj.Status = 0;
                    LaundryManObj.RoleID = roleBs.GetRoleIdByName("User").RoleID;
                    LaundryManObj.Keydate = DateTime.Now;
                    LaundryManObj.Flag = "A";
                    NewLaundryManBs.Insert(LaundryManObj);
                    ViewData["Message"] = "Record save successfully";
                    return View();
                }
                else
                {
                    ViewData["Message"] = "Password does not matches";
                    return View(LaundryManObj);
                }
            }
            else
            {
                return View();
            }
        }


        // GET: Security/Registration/Create
        public ActionResult CustomerRegistration()
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            return View(NewCustomerBs.GetLastRegisteredCustomer());
        }

        // POST: Security/Registration/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CustomerRegistration(FormCollection collection)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }

            if (ModelState.IsValid)
            {
                CustomerObj.Surname = collection["Surname"];
                CustomerObj.Othername = collection["Othername"];
                CustomerObj.Sex = collection["Sex"];
                CustomerObj.CustomerPhone = collection["CustomerPhone"];
                CustomerObj.Address = collection["Address"];
                CustomerObj.EmailAddress = collection["EmailAddress"];
                CustomerObj.Password = "default";

                var result = NewCustomerBs.GetByPhoneNumber(CustomerObj.CustomerPhone);

                if (result != null)
                {
                    ViewData["Message"] = "Phone Number already exist";
                    return View(CustomerObj);
                }
                if (HashHelper.IsValidEmailFormat(CustomerObj.EmailAddress) == true)
                {

                    var EmailSearchrResult = NewCustomerBs.GetByEmail(CustomerObj.EmailAddress);

                    if (EmailSearchrResult != null)
                    {
                        ViewData["Message"] = "Email address already exist";
                        return View(CustomerObj);
                    }
                }
                else
                {
                    ViewData["Message"] = "Invalid E-mail Address";
                }
               
                CustomerObj.CustomerId = Guid.NewGuid().ToString("N"); ;
                CustomerObj.Reg_Status = "A";
                CustomerObj.UserId = ViewBag.UserId;
                CustomerObj.Keydate = DateTime.Now;
                CustomerObj.Flag = "A";
                NewCustomerBs.Insert(CustomerObj);
                ViewData["Message"] = "Customer registered successfully";
                return View(NewCustomerBs.GetLastRegisteredCustomer());
            }
            else
            {
                return View();
            }
            //return View();
        }

        public ActionResult UpdateCustomer(string phonenumber="")
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }

            return View(NewCustomerBs.GetByPhoneNumber(phonenumber));
        }

        // POST: Security/Registration/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateCustomer(FormCollection collection)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }

            if (ModelState.IsValid)
            {
                CustomerObj.CustomerId = collection["CustomerId"];
                CustomerObj.Surname = collection["Surname"];
                CustomerObj.Othername = collection["Othername"];
                CustomerObj.EmailAddress = collection["EmailAddress"];
                CustomerObj.CustomerPhone = collection["CustomerPhone"];
                CustomerObj.Address = collection["Address"];
                CustomerObj.Reg_Status = "A";
                CustomerObj.UserId = ViewBag.UserId;
                CustomerObj.Keydate = DateTime.Now;
                CustomerObj.Flag = "C";
                NewCustomerBs.Update(CustomerObj);
                ViewData["Message"] = "Customer record updated successfully";
                return View();
            }
            else
            {
                return View();
            }
            //return View();
        }
        public ActionResult CompanyRegistration()
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            return View(NewCompanyDetailBs.ListAll());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CompanyRegistration(FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ViewBag.UserId = Session["Username"].ToString();
                }
                catch
                {
                    Session["ConfirmLogin"] = "You must login first";
                    return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
                }

                if (collection["Company_Password"] == collection["ConfirmPassword"])
                {

                    CompanyDetailObj.Company_Name = collection["Company_Name"];
                    CompanyDetailObj.Company_ShortName = collection["Company_ShortName"];
                    CompanyDetailObj.Company_Address = collection["Company_Address"];
                    CompanyDetailObj.Company_Phone1 = collection["Company_Phone1"];
                    CompanyDetailObj.Company_Phone2 = collection["Company_Phone2"];
                    CompanyDetailObj.Company_Email = collection["Company_Email"];
                    CompanyDetailObj.Company_Username = collection["Company_Username"];
                    CompanyDetailObj.Company_Password = collection["Company_Password"];
                    CompanyDetailObj.Company_UserId = "admin";
                    CompanyDetailObj.Company_KeyDate = DateTime.Now;
                    CompanyDetailObj.Company_Flag = "A";
                    NewCompanyDetailBs.Insert(CompanyDetailObj);
                    ViewData["Message"] = "Record save successfully";
                    return View(NewCompanyDetailBs.ListAll());
                }
                else
                {
                    ViewData["Message"] = "Password does not matches";
                    //return RedirectToAction("LaundryManRegistration");
                    return View(CompanyDetailObj);
                }
            }
            else
            {
                return View();
            }
            //return View();
        }

        [HttpGet]
        public ActionResult CompanyRegUpdate(int id = 0)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            return View(NewCompanyDetailBs.GetById(id));
        }

        [HttpPost]
        public ActionResult CompanyRegUpdate(int id, FormCollection collection)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }

            CompanyDetailObj.Company_Id = Convert.ToInt32(collection["Company_Id"]);
            CompanyDetailObj.Company_Name = collection["Company_Name"];
            CompanyDetailObj.Company_ShortName = collection["Company_ShortName"];
            CompanyDetailObj.Company_Address = collection["Company_Address"];
            CompanyDetailObj.Company_Phone1 = collection["Company_Phone1"];
            CompanyDetailObj.Company_Phone2 = collection["Company_Phone2"];
            CompanyDetailObj.Company_Email = collection["Company_Email"];
            //CompanyDetailObj.Company_Username = collection["Company_Username"];
            //CompanyDetailObj.Company_Password = collection["Company_Password"];
            CompanyDetailObj.Company_UserId = "admin";
            CompanyDetailObj.Company_Flag = "C";
            NewCompanyDetailBs.Update(CompanyDetailObj);
            ViewData["Message"] = "Record save successfully";
            return RedirectToAction("CompanyRegistration");
        }

        [HttpGet]
        public ActionResult UploadCompanyLogo()
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            var result = NewCompanyLogoBs.GetCompanyLogo();
            if (result != null)
            {
                return View(result);
            }
            return View();
        }

        [HttpPost]
        public ActionResult UploadCompanyLogo(FormCollection collection, HttpPostedFileBase image)
        {
            try
            {
                ViewBag.Username = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }

            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    CompanyLogoObj.Username = ViewBag.Username;
                    postedImage = image;
                    byteImageData = ByteImage(postedImage.FileName, new string[] { ".gif", ".jpg", ".bmp", ".png", ".jpeg" }, image);
                    CompanyLogoObj.Logo = byteImageData;
                    var result = NewCompanyLogoBs.GetCompanyLogo();
                    if (result == null)
                    {
                        CompanyLogoObj.CompanyLogoId = Guid.NewGuid().ToString("N");
                        CompanyLogoObj.Key_Date = DateTime.Today;
                        CompanyLogoObj.Flag = "A";
                        NewCompanyLogoBs.Insert(CompanyLogoObj);
                        return RedirectToAction("UploadCompanyLogo");
                    }
                    else
                    {
                        CompanyLogoObj.Flag = "C";
                        NewCompanyLogoBs.Update(CompanyLogoObj);
                        return RedirectToAction("UploadCompanyLogo");
                    }
                }
            }
            else
            {
                return View(CompanyLogoObj);
            }
            return View();
        }


        private static byte[] ReadImage(string p_postedImageFileName, string[] p_fileType)
        {
            bool isValidFileType = false;
            try
            {
                FileInfo file = new FileInfo(p_postedImageFileName);
                foreach (string strExtensionType in p_fileType)
                {
                    if (strExtensionType == file.Extension)
                    {
                        isValidFileType = true;
                        break;
                    }
                }
                if (isValidFileType)
                {
                    FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    byte[] image = br.ReadBytes((int)fs.Length);
                    br.Close();
                    fs.Close();
                    return image;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static byte[] ByteImage(string p_postedImageFileName, string[] p_fileType, HttpPostedFileBase img)
        {
            bool isValidFileType = false;
            try
            {
                FileInfo file = new FileInfo(p_postedImageFileName);
                foreach (string strExtensionType in p_fileType)
                {
                    if (strExtensionType == file.Extension)
                    {
                        isValidFileType = true;
                        break;
                    }
                }
                if (isValidFileType)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        img.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                        return array;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public JsonResult VerifyUsername(string id)
        {
            var result = NewLaundryManBs.VerifyUsername(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VerifyCompanyEmail(string email)
        {
            var result = NewCompanyDetailBs.VerifyCompanyEmail(email);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VerifyCustomerPhoneNumber(string phone)
        {
            var result = NewCustomerBs.GetByPhoneNumber(phone);
            if (result != null)
                message = "Customer phone number already exist";
            else
                message = "";

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VerifyCustomerEmail(string email)
        {
            var result = NewCustomerBs.GetByEmail(email);
            if (result != null)
                message = "Customer email address already exist";
            else
                message = "";
            return Json(message, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Security/Registration/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Security/Registration/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Security/Registration/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [HttpGet]
        public ActionResult AdminReg()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminReg(FormCollection collection)
        {
            return View();
        }

        //public ActionResult UploadLogo()
        //{
        //    try
        //    {
        //        ViewBag.UserId = Session["Username"].ToString();
        //    }
        //    catch
        //    {
        //        Session["ConfirmLogin"] = "You must login first";
        //        return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
        //    }
        //    var result = NewCompanyDetailBs.GetCompanyDetail();
        //    if (result != null)
        //    {
        //        return View(result);
        //    }
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult UploadLogo(FormCollection collection, HttpPostedFileBase image)
        //{
        //    try
        //    {
        //        ViewBag.Username = Session["Username"].ToString();
        //    }
        //    catch
        //    {
        //        Session["ConfirmLogin"] = "You must login first";
        //        return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        if (image != null)
        //        {
        //            CompanyDetailObj.Company_Username = ViewBag.Username;
        //            postedImage = image;
        //            byteImageData = ByteImage(postedImage.FileName, new string[] { ".gif", ".jpg", ".bmp" }, image);
        //            CompanyDetailObj.Logo = byteImageData;
        //            var result = NewCompanyDetailBs.GetCompanyDetail();
        //            if (result == null)
        //            {
        //                CompanyDetailObj.Company_Flag = "C";
        //                NewCompanyLogoBs.Update(CompanyDetailObj);
        //                return RedirectToAction("UploadCompanyLogo");
        //            }
        //        }
        //    }
        //    else
        //    {
        //        return View(CompanyLogoObj);
        //    }
        //    return View();
        //}
    }
}
