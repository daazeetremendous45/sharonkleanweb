﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Laundry.Model;
using Laundry.BLL;
using System.Threading.Tasks;

namespace Laundry.Web.Areas.Admin.Controllers
{
    public class ClothingUpdateController : Controller
    {
        private Clothing ClothingObj = new Clothing();
        private ClothingBs NewClothingBs = new ClothingBs();
        private GarmentBs garmentBs = new GarmentBs();
        private Garment garmentObj = new Garment();
        private CodesBs codesBs = new CodesBs();
        private PriceDetail PriceDetailObj = new PriceDetail();
        private PriceDetailBs priceDetailBs = new PriceDetailBs();
        public ActionResult PriceList()
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            return View(NewClothingBs.ListAll());
        }

        // GET: Admin/ClothingUpdate/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/ClothingUpdate/Create
        public ActionResult Create()
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }

            return View();
        }

        // POST: Admin/ClothingUpdate/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection collection)
        {

            //try
            //{
            if (ModelState.IsValid)
            {
                // TODO: Add insert logic here
                //ClothingObj.ClothId = collection["ClothId"];
                ClothingObj.ClothDesc = collection["ClothDesc"];
                ClothingObj.Amount = Convert.ToDouble(collection["Amount"]);
                ClothingObj.GenderType = collection["GenderType"];
                ClothingObj.ServiceType = collection["ServiceType"];
                ClothingObj.UserId = Session["Username"].ToString();
                ClothingObj.Keydate = DateTime.Now;
                string flag = collection["Flag"];
                if (flag == "false")
                {
                    ClothingObj.Flag = "A";
                    NewClothingBs.Insert(ClothingObj);
                    ViewData["Message"] = "Record save successfully";
                }
                else
                {
                    ClothingObj.Flag = "C";
                    NewClothingBs.Update(ClothingObj);
                    ViewData["Message"] = "Record updated successfully";
                }

                //return View();

                // return RedirectToAction("Index");
            }
            return View();
            //}
            //catch
            //{
            //    return View();
            //}
        }


        public async Task<ActionResult> ClothEntry()
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            ViewBag.GenderType = new SelectList(codesBs.GetByCodeType("GenderType"), "Codes_Val", "Codes_Desc");
            ViewBag.ServiceType = new SelectList(codesBs.GetByCodeType("ServiceType"), "Codes_Val", "Codes_Desc");
            return View(NewClothingBs.ListAll());
        }

        // POST: Admin/ClothingUpdate/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ClothEntry(FormCollection collection)
        {

            //try
            //{
            if (ModelState.IsValid)
            {
                // TODO: Add insert logic here
                ViewBag.GenderType = new SelectList(codesBs.GetByCodeType("GenderType"), "Codes_Val", "Codes_Desc");
                ViewBag.ServiceType = new SelectList(codesBs.GetByCodeType("ServiceType"), "Codes_Val", "Codes_Desc");

                double amount;
                //if (collection["ClothId"] == "")
                //{
                //    ViewData["Message"] = "Cloth Id must not be empty";
                //    return View(NewClothingBs.ListAll());
                //}
                if (collection["ClothDesc"] == "")
                {
                    ViewData["Message"] = "Cloth description must not be empty";
                    return View(NewClothingBs.ListAll());
                }
                else if (collection["Amount"] == "")
                {
                    ViewData["Message"] = "Amount must not be empty";
                    return View(NewClothingBs.ListAll());
                }
                else if (!Double.TryParse(collection["Amount"], out amount))
                {
                    ViewData["Message"] = "Amount must be numeric";
                    return View(NewClothingBs.ListAll());
                }
                ClothingObj.ClothingID = Convert.ToInt32(collection["ClothingID"]);
                ClothingObj.ClothDesc = collection["ClothDesc"];
                ClothingObj.GenderType = collection["GenderType"];
                ClothingObj.ServiceType = collection["ServiceType"];
                ClothingObj.Amount = Convert.ToDouble(collection["Amount"]);
                ClothingObj.UserId = "admin";
                ClothingObj.Keydate = DateTime.Now;
                ClothingObj.Flag = "A";


                var checkExistingGarment = await NewClothingBs.GetByGarmentGenderServiceTypeAsync
                    (ClothingObj.ClothDesc, ClothingObj.GenderType, ClothingObj.ServiceType);
                if (checkExistingGarment != null)
                {
                    ViewData["Message"] = $"Record already exist";
                    return View(NewClothingBs.ListAll());
                }

                //var ExistingClothId = NewClothingBs.GetByClothValue(ClothingObj.ClothId);
                //if (ExistingClothId != null)
                //{
                //    ViewData["Message"] = "Cloth ID already exist for descrption " + ExistingClothId.ClothDesc.ToString();
                //    return View(NewClothingBs.ListAll());
                //}

                //var ExistingClothDesc = NewClothingBs.GetByClothDesc(ClothingObj.ClothDesc);
                //if (ExistingClothDesc != null)
                //{
                //    ViewData["Message"] = "Cloth description already exist for cloth id " + ExistingClothDesc.ClothId.ToString();
                //    return View(NewClothingBs.ListAll());
                //}

                NewClothingBs.Insert(ClothingObj);

            }
            return View(NewClothingBs.ListAll());
        }

        [HttpGet]
        public ActionResult ClothUpdate(int id = 0)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            var result = NewClothingBs.GetById(id);
            ViewBag.GenderTypes = new SelectList(codesBs.GetByCodeType("GenderType"), "Codes_Val", "Codes_Desc");
            ViewBag.ServiceTypes = new SelectList(codesBs.GetByCodeType("ServiceType"), "Codes_Val", "Codes_Desc");

            return View(result);
        }

        [HttpPost]
        public ActionResult ClothUpdate(int id, FormCollection collection)
        {
            double amount;
            try
            {
                if (collection["ClothDesc"] == "")
                {
                    ViewData["Message"] = "Cloth description must not be empty";
                    return RedirectToAction("ClothEntry");
                }
                else if (collection["Amount"] == "")
                {
                    ViewData["Message"] = "Amount must not be empty";
                    return RedirectToAction("ClothEntry");
                }
                else if (!Double.TryParse(collection["Amount"], out amount))
                {
                    ViewData["Message"] = "Amount must be numeric";
                    return RedirectToAction("ClothEntry");
                }
                ClothingObj.ClothingID = id;
                ClothingObj.ClothDesc = collection["ClothDesc"];
                ClothingObj.Amount = Convert.ToDouble(collection["Amount"]);
                ClothingObj.UserId = Session["Username"].ToString();
                ClothingObj.Keydate = DateTime.Now;
                ClothingObj.UserId = Session["Username"].ToString();
                ClothingObj.Flag = "C";

                //var ExistingCode = NewCodesBs.GetByCodeValue(CodesObj.Codes_Val);
                //if (ExistingCode != null)
                //{
                //    ViewData["Message"] = "Code value already exist for descrption " + ExistingCode.Codes_Desc.ToString();
                //    return RedirectToAction("CodesEntry", new { code_type = CodesObj.Codes_Type, code_type_desc = Codes_Type_Desc });
                //}

                //var ExistingDesc = NewClothingBs.GetByClothDesc(ClothingObj.ClothDesc);
                //if (ExistingDesc != null)
                //{
                //    if (ClothingObj.ClothDesc.ToString() == ExistingDesc.ClothDesc.ToString())
                //    {
                //        //ViewData["Message"] = "Cloth description already exist for cloth value " + ExistingDesc.ClothId.ToString();
                //        return RedirectToAction("ClothEntry");
                //    }
                //}

                //var ExistingDesc = NewClothingBs.ClothDescripton(ClothingObj.ClothId,ClothingObj.ClothDesc);
                //if (ExistingDesc != null)
                //{
                //        ViewData["Message"] = "Cloth description already exist";
                //        return RedirectToAction("ClothEntry");
                //}

                NewClothingBs.Update(ClothingObj);
                ViewData["Message"] = "Record updated successfully";

                return RedirectToAction("ClothEntry");
            }
            catch (Exception ex)
            {
                ViewData["Message"] = ex.Message;
                return RedirectToAction("ClothEntry");
            }
        }

        public JsonResult Search(int id, string key)
        {
            var result = NewClothingBs.GetById(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GarmentEntry()
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            return View(garmentBs.ListAll());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GarmentEntry(FormCollection collection)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }

            if (ModelState.IsValid)
            {
                // TODO: Add insert logic here
                ViewBag.GenderType = new SelectList(codesBs.GetByCodeType("GenderType"), "Codes_Val", "Codes_Desc");
                ViewBag.ServiceType = new SelectList(codesBs.GetByCodeType("ServiceType"), "Codes_Val", "Codes_Desc");


                if (collection["GarmentName"] == "")
                {
                    ViewData["Message"] = "Garment name must not be empty";
                    return View(garmentBs.ListAll());
                }

                garmentObj.GarmentName = collection["GarmentName"];
                garmentObj.CreatedBy = garmentObj.ModifiedBy = ViewBag.UserId;
                garmentObj.CreatedOn = garmentObj.ModifiedOn = DateTime.Now;
                garmentObj.Status = 1;


                var checkExistingGarment = garmentBs.GetByName(garmentObj.GarmentName);
                if (checkExistingGarment != null)
                {
                    ViewData["Message"] = $"Garment {garmentObj.GarmentName} already exist";
                    return View(garmentBs.ListAll());
                }

                garmentBs.Insert(garmentObj);
            }
            return View(garmentBs.ListAll());
        }

        public async Task<ActionResult> PriceEntry()
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }

            ViewBag.Garments = new SelectList(garmentBs.ListAll(), "GarmentID", "GarmentName");
            ViewBag.CleaningCategory = new SelectList(codesBs.GetByCodeType("CleaningCategory"), "Codes_Val", "Codes_Desc");
            ViewBag.ServiceTypes = new SelectList(codesBs.GetByCodeType("ServiceType"), "Codes_Val", "Codes_Desc");
            return View(priceDetailBs.ListAll());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PriceEntry(FormCollection collection)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }


            if (ModelState.IsValid)
            {
                ViewBag.Garments = new SelectList(garmentBs.ListAll(), "GarmentID", "GarmentName");
                ViewBag.CleaningCategory = new SelectList(codesBs.GetByCodeType("CleaningCategory"), "Codes_Val", "Codes_Desc");
                ViewBag.ServiceTypes = new SelectList(codesBs.GetByCodeType("ServiceType"), "Codes_Val", "Codes_Desc");

                double amount;

                if (collection["GarmentID"] == "")
                {
                    ViewData["Message"] = "Please select garment";
                    return View(priceDetailBs.ListAll());
                }
                else if (collection["CleaningCategory"] == "")
                    {
                        ViewData["Message"] = "Please select cleaning category";
                        return View(priceDetailBs.ListAll());
                    }
                else if(collection["CleaningCategory"] != "3" && collection["ServiceType"] == "")//if not household
                {
                    ViewData["Message"] = "Please select service type";
                    return View(priceDetailBs.ListAll());
                }
                    else if (collection["Amount"] == "")
                {
                    ViewData["Message"] = "Amount must not be empty";
                    return View(priceDetailBs.ListAll());
                }
                else if (!Double.TryParse(collection["Amount"], out amount))
                {
                    ViewData["Message"] = "Amount must be numeric";
                    return View(priceDetailBs.ListAll());
                }
                PriceDetailObj.GarmentID = Convert.ToInt32(collection["GarmentID"]);
                PriceDetailObj.CleaningCategory = collection["CleaningCategory"];
                PriceDetailObj.ServiceType = collection["ServiceType"];
                PriceDetailObj.Amount = Convert.ToDouble(collection["Amount"]);
                PriceDetailObj.UserId = ViewBag.UserId;
                PriceDetailObj.Keydate = DateTime.Now;
                PriceDetailObj.Flag = "A";

                object checkExistingGarment;
                if (!string.IsNullOrEmpty(PriceDetailObj.ServiceType))
                {
                    checkExistingGarment = priceDetailBs.GetPriceByGarmentCleaningCatServiceType
                   (PriceDetailObj.GarmentID, PriceDetailObj.CleaningCategory, PriceDetailObj.ServiceType);

                }
                else
                {
                    checkExistingGarment = priceDetailBs.GetPriceByGarmentCleaningCat
                  (PriceDetailObj.GarmentID, PriceDetailObj.CleaningCategory);

                }
                if (checkExistingGarment != null)
                {
                    ViewData["Message"] = $"Record already exist";
                    return View(priceDetailBs.ListAll());
                }

                priceDetailBs.Insert(PriceDetailObj);
            }
            return View(priceDetailBs.ListAll());
        }

        [HttpGet]
        public ActionResult PriceUpdate(int id = 0)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            var result = priceDetailBs.GetById(id);

            ViewBag.Garments = new SelectList(garmentBs.ListAll(), "GarmentID", "GarmentName");
            ViewBag.CleaningCategories = new SelectList(codesBs.GetByCodeType("CleaningCategory"), "Codes_Val", "Codes_Desc");
            ViewBag.ServiceTypes = new SelectList(codesBs.GetByCodeType("ServiceType"), "Codes_Val", "Codes_Desc");

            return View(result);
        }

        [HttpPost]
        public ActionResult PriceUpdate(int id, FormCollection collection)
        {
            double amount;
            try
            {
               
                if (collection["Amount"] == "")
                {
                    ViewData["Message"] = "Amount must not be empty";
                    return RedirectToAction("PriceEntry");
                }
                else if (!Double.TryParse(collection["Amount"], out amount))
                {
                    ViewData["Message"] = "Amount must be numeric";
                    return RedirectToAction("PriceEntry");
                }
                PriceDetailObj.PriceDetailID = id;
                PriceDetailObj.Amount = Convert.ToDouble(collection["Amount"]);
                PriceDetailObj.UserId = Session["Username"].ToString();
                PriceDetailObj.Keydate = DateTime.Now;
                PriceDetailObj.Flag = "C";

               
                priceDetailBs.Update(PriceDetailObj);
                ViewData["Message"] = "Record updated successfully";

                return RedirectToAction("PriceEntry");
            }
            catch (Exception ex)
            {
                ViewData["Message"] = ex.Message;
                return RedirectToAction("PriceEntry");
            }
        }

    }
}
