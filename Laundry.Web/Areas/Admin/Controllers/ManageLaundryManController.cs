﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Laundry.Model;
using Laundry.BLL;

namespace Laundry.Web.Areas.Admin.Controllers
{
    public class ManageLaundryManController : Controller
    {
        LaundryManBs NewLaundryManBs = new LaundryManBs();
        RoleBs roleBs = new RoleBs();
        BranchBs branchBs = new BranchBs();
        LaundryMan LaundryManObj = new LaundryMan();

        // GET: Admin/ManageLaundryMan

        public ActionResult ListLaundryMan(string status)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            //if (status == null)
            //{
            //    status = "P";
            //}
            //ViewBag.status = status;
            //if (status == "L")
            //{
            //    return View(NewLaundryManBs.ListAll());
            //}

            return View(NewLaundryManBs.ListAll());
            //return View(NewLaundryManBs.ListAllByStatus(status));
        }

        //public ActionResult UpdateStatus(string username, string status)
        //{
        //    try
        //    {
        //        ViewBag.UserId = Session["Username"].ToString();
        //    }
        //    catch
        //    {
        //        Session["ConfirmLogin"] = "You must login first";
        //        return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
        //    }
        //    var result = NewLaundryManBs.UpdateStatus(username, status);
        //    return RedirectToAction("ListLaundryMan");
        //}

        public ActionResult UpdateUser(string username)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            ViewBag.Roles = new SelectList(roleBs.GetAllRoles(), "RoleID", "RoleName");
            ViewBag.Branch = new SelectList(branchBs.ListAll(), "BranchID", "BranchName");
            var result = NewLaundryManBs.GetByUsername(username);
            return View(result);
        }

        [HttpPost]
        public ActionResult UpdateUser(FormCollection collection)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            LaundryManObj.LaundryManID = Convert.ToInt32(collection["LaundryManID"]);
            LaundryManObj.Status = Convert.ToInt32(collection["Status"]);
            LaundryManObj.RoleID = Convert.ToInt32(collection["RoleID"]);
            LaundryManObj.BranchID = Convert.ToInt32(collection["BranchID"]);

            // LaundryManObj.Password = collection["NewPassword"];
            ViewBag.Roles = new SelectList(roleBs.GetAllRoles(), "RoleID", "RoleName");
            ViewBag.Branch = new SelectList(branchBs.ListAll(), "BranchID", "BranchName");
            //if (collection["NewPassword"] != "" && collection["NewConfirmPassword"] != "")
            //{
            //    if (collection["NewPassword"] != collection["NewConfirmPassword"])
            //    {
            //        ViewData["Message"] = "Password does not matches";
            //        return View(LaundryManObj);
            //    }
            //}

            LaundryManObj.Flag = "C";
            NewLaundryManBs.UpdateUserRoleStatus(LaundryManObj);
            ViewData["Message"] = "Record updated successfully";
            return View(NewLaundryManBs.GetById(LaundryManObj.LaundryManID));
        }

    }
}