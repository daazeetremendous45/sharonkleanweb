﻿using Laundry.BLL;
using Laundry.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Laundry.Web.Areas.Admin.Controllers
{
    public class BranchController : Controller
    {
        Branch BranchObj = new Branch();
        BranchBs branchBs = new BranchBs();
        CompanyDetailBs companyDetailBs = new CompanyDetailBs();
        // GET: Admin/Branch
        public ActionResult BranchEntry()
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }

            ViewBag.Branches = new SelectList(branchBs.ListAll(), "BranchID", "BranchName");
            return View(branchBs.ListAll());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BranchEntry(FormCollection collection)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }


            if (ModelState.IsValid)
            {
                ViewBag.Branches = new SelectList(branchBs.ListAll(), "BranchID", "BranchName");

                var companyDetail = companyDetailBs.GetCompanyDetail();
                if (collection["Name"] == "")
                {
                    ViewData["Message"] = "Please enter branch name";
                    return View(branchBs.ListAll());
                }
                else if (collection["Address"] == "")
                {
                    ViewData["Message"] = "Please enter address";
                    return View(branchBs.ListAll());
                }
                else if (collection["PhoneNumber1"] == "")
                {
                    ViewData["Message"] = "Please enter first phone number";
                    return View(branchBs.ListAll());
                }
                else if (companyDetail == null)
                {
                    ViewData["Message"] = "Please set up company before branch";
                    return View(branchBs.ListAll());
                }
                BranchObj.BranchName = collection["Name"];
                BranchObj.Address = collection["Address"];
                BranchObj.PhoneNumber1 = collection["PhoneNumber1"];
                BranchObj.PhoneNumber2 = collection["PhoneNumber2"];
                BranchObj.Company_Id = companyDetail.Company_Id;
                BranchObj.CreatedBy = BranchObj.ModifiedBy = ViewBag.UserId;
                BranchObj.CreatedOn = BranchObj.ModifiedOn = DateTime.Now;
                BranchObj.Status = 1;

                var result = branchBs.GetByName(BranchObj.BranchName);
                if (result != null)
                {
                    ViewData["Message"] = $"Branch {BranchObj.BranchName} already exist";
                    return View(branchBs.ListAll());
                }
                branchBs.Insert(BranchObj);
            }
            return View(branchBs.ListAll());
        }

        [HttpGet]
        public ActionResult BranchUpdate(int id = 0)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            var result = branchBs.GetById(id);

            ViewBag.Branches = new SelectList(branchBs.ListAll(), "BranchID", "BranchName");

            return View(result);
        }

        [HttpPost]
        public ActionResult BranchUpdate(int id, FormCollection collection)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            try
            {

                if (collection["Address"] == "")
                {
                    ViewData["Message"] = "Please enter address";
                    return View(branchBs.ListAll());
                }
                else if (collection["PhoneNumber1"] == "")
                {
                    ViewData["Message"] = "Please enter first phone number";
                    return View(branchBs.ListAll());
                }
                BranchObj.BranchID = id;
                BranchObj.Address = collection["Address"];
                BranchObj.PhoneNumber1 = collection["PhoneNumber1"];
                BranchObj.PhoneNumber2 = collection["PhoneNumber2"];
                BranchObj.ModifiedBy = ViewBag.UserId;
                BranchObj.ModifiedOn = DateTime.Now;



                branchBs.Update(BranchObj);
                ViewData["Message"] = "Record updated successfully";

                return RedirectToAction("BranchEntry");
            }
            catch (Exception ex)
            {
                ViewData["Message"] = ex.Message;
                return RedirectToAction("BranchEntry");
            }
        }
    }
}