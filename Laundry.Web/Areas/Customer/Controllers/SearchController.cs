﻿using Laundry.BLL;
using Laundry.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Laundry.Web.Areas.Customer.Controllers
{
    public class SearchController : Controller
    {
        // GET: Customer/Search
        CustomerBs NewCustomerBs = new CustomerBs();
        Model.Customer CustomerObj = new Model.Customer();
        public ActionResult Index()
        {
            return View();
        }

        //[HttpGet]
        //public ActionResult Search()
        //{
        //    try
        //    {
        //        ViewBag.UserId = Session["Username"].ToString();
        //    }
        //    catch
        //    {
        //        Session["ConfirmLogin"] = "You must login first";
        //        return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
        //    }
        //    return View();
        //}

        //// [ValidateAntiForgeryToken]
        //[HttpPost]
        //public ActionResult Search(string type, string value, string startdate, string enddate)
        //{


        //    DateTime FinalStartDate = DateTime.Today, FinalEndDate = DateTime.Today;
        //    if (startdate != "" && DateTime.TryParse(startdate, out FinalStartDate))
        //    {
        //        //FinalStartDate = Convert.ToDateTime(ConvertToDateFormat(startdate));
        //        FinalStartDate = Convert.ToDateTime(startdate);
        //    }

        //    if (enddate != "" && DateTime.TryParse(enddate, out FinalEndDate))
        //    {
        //        FinalEndDate = Convert.ToDateTime(enddate);
        //    }

        //    return View(NewTransactionBs.Search(type, value, FinalStartDate, FinalEndDate));
        //}

        //[HttpGet]
        //public ActionResult _CustomerSearch()
        //{
        //    try
        //    {
        //        ViewBag.UserId = Session["Username"].ToString();
        //    }
        //    catch
        //    {
        //        Session["ConfirmLogin"] = "You must login first";
        //        return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
        //    }
        //    return PartialView();
        //}

        //[ValidateAntiForgeryToken]
        //[HttpPost]
        //public ActionResult _AddItem(FormCollection collection)
        //{
        //    var Clothes = NewClothingBS.ListAll();
        //    ViewBag.Code_Desc = new SelectList(Clothes, "ClothId", "ClothDesc");
        //    ViewBag.Color = new SelectList(NewCodesBs.GetByCodeType("Cod001"), "Codes_Val", "Codes_Desc");
        //    ViewBag.CollectionType = new SelectList(NewCodesBs.GetByCodeType("Cod002"), "Codes_Val", "Codes_Desc");
        //    ViewBag.StarchLevel = new SelectList(NewCodesBs.GetByCodeType("Cod003"), "Codes_Val", "Codes_Desc");
        //    ViewBag.PressingOption = new SelectList(NewCodesBs.GetByCodeType("Cod004"), "Codes_Val", "Codes_Desc");

        //    return PartialView();
        //}

        public JsonResult GetCustomerByPhoneNumber(string phone)
        {
            var result = NewCustomerBs.GetByPhoneNumber(phone);
            Model.Customer MyResult = new Model.Customer();

            if (result != null)
            {
                MyResult.Surname = result.Surname;
                MyResult.Othername = result.Othername;
                MyResult.EmailAddress = result.EmailAddress;
                MyResult.Sex = result.Sex;
                MyResult.Address = result.Address;
                MyResult.CustomerId = result.CustomerId;
            }
            return Json(MyResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerByEmail(string email)
        {
            var result = NewCustomerBs.GetByEmail(email);
            Model.Customer MyResult = new Model.Customer();

            if (result != null)
            {
                MyResult.Surname = result.Surname;
                MyResult.Othername = result.Othername;
                MyResult.CustomerPhone = result.CustomerPhone;
            }
            return Json(MyResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CustomerSearch(string name, string PageNo = null)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }

            int pageNo = int.Parse(PageNo == null ? "1" : PageNo);
            ViewBag.CurrentPage = pageNo;
            ViewBag.name = name;

            Session["NameSearch"] = name;
            var CustomerSearchResult = NewCustomerBs.SearchCustomerByName(name);
            ViewBag.TotalPages = NewCustomerBs.CustomerSearchTotalPages(CustomerSearchResult.Count());
            return View(CustomerSearchResult.Skip((pageNo - 1) * 10).Take(10));
        }

        public ActionResult Overview(string phonenumber)
        {
            try
            {
                ViewBag.UserId = Session["Username"].ToString();
            }
            catch
            {
                Session["ConfirmLogin"] = "You must login first";
                return RedirectToAction("Login", new { Area = "Security", Controller = "Access" });
            }
            return View(NewCustomerBs.GetByPhoneNumber(phonenumber));
        }
    }
}