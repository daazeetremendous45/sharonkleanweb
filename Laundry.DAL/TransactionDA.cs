﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.Model;
using System.Data.Entity;

namespace Laundry.DAL
{
    public class TransactionDA
    {
        private string message;
        private LaundryContext context = new LaundryContext();

        public IEnumerable<Transaction> GetAll()
        {
            return context.Transactions.ToList();
        }

        public Transaction GetById(int id)
        {
            return context.Transactions.Where(c => c.TransactionId == id).FirstOrDefault();
        }
        public void Insert(Transaction TransactionDAObj)
        {
            context.Transactions.Add(TransactionDAObj);
            context.SaveChanges();
        }

        public void Update(Transaction TransactionDAObj)
        {
            context.Entry(TransactionDAObj).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var search = context.Transactions.Where(c => c.TransactionId == id).FirstOrDefault();
            context.Transactions.Remove(search);
            context.SaveChanges();
        }

        public int GetLastTransNo(string TNo)
        {
            int LastTransNo = 0;
            var search = from p in context.Transactions
                         where p.TransactionNo.StartsWith(TNo)
                         orderby p.TransactionId descending
                         //orderby p.TransactionNo descending //String Ordering does not yield accurate result
                         select p;
            var result = search.FirstOrDefault();

            if (result != null)
            {
                string[] SplitResult = result.TransactionNo.Split('/');
                LastTransNo = Convert.ToInt32(SplitResult[2]);
            }
            return LastTransNo;
        }

        public IEnumerable<Transaction> GetByTransactionNo(string TransNo)
        {
            return context.Transactions.Where(c => c.TransactionNo == TransNo).Include(b => b.Customer).ToList();
        }

        public IEnumerable<Transaction> Search(string type, string value, DateTime startdate, DateTime enddate)
        {
            IEnumerable<Transaction> result = null;
            if (type == "TransNo")
            {
                result = context.Transactions.Where(c => c.TransactionNo.StartsWith(value)).ToList();
            }
            else if (type == "Name")
            {
                //  result = context.Transactions.Where(c => c.CustomerName == value).ToList();
                result = GetByCustomerName(value);
            }
            else if (type == "Date")
            {
                result = context.Transactions.Where(c => c.KeyDate >= startdate).Where(c => c.KeyDate <= enddate).ToList();
            }
            else if (type == "Type")
            {
                result = context.Transactions.Where(c => c.ClothCode == value).ToList();
            }
            return result;
        }

        public void UpdateClothStatus(int Transid, string status)
        {
            var search = context.Transactions.Where(c => c.TransactionId == Transid).FirstOrDefault();
            //if (status == "C")
            //{
            //    if (search.ClothStatus == "R")
            //    {
            //        if (search.TotalCostAmount != search.AmountPaid)
            //        {
            //            message = "Customer has not balance is payment, please update payment.";
            //            return message;
            //        }
            //        else
            //        {
            //            search.ClothStatus = status;
            //            context.SaveChanges();
            //            message = "Status updated to Collected successfully";
            //            return message;
            //        }
            //    }
            //    else if (search.ClothStatus =="N")
            //    {
            //        message = "Cloth is not ready for collection, please check cloth status";
            //        return message;
            //    }
            //    else
            //    {
            //        message = "Cloth has already been collected";
            //        return message;
            //    }
            //}

            search.ClothStatus = status;
            context.SaveChanges();
            //message = "Status updated to Ready successfully";
            // return message;
        }

        public Transaction GetSingleTransaction(string TransNo)
        {
            return context.Transactions.Where(c => c.TransactionNo == TransNo).FirstOrDefault();
        }

        public IEnumerable<Transaction> GetByCustomerName(string name)
        {

            return context.Transactions.Where(c => c.Customer.Surname.StartsWith(name) || c.Customer.Surname.EndsWith(name)).Include("Customer").ToList();
        }

        //public IEnumerable<Transaction> GetTransactionByPhoneNumber(string phonenumber)
        //{

        //    return context.Transactions.Where(c => c.CustomerPhone == phonenumber).Include("Customer").ToList();
        //}


        public IEnumerable<IGrouping<TransactionHistory, Transaction>> GetTransactionByPhoneNumber(string phonenumber)
        {

            return context.Transactions.Where(c => c.CustomerPhone == phonenumber).Include("Customer")
               .GroupBy(myHistory => new TransactionHistory{ TransactionNo=myHistory.TransactionNo
               , CustomerTag=myHistory.CustomerTag,
               CustomerPhone=myHistory.CustomerPhone,
               TotalCostAmount=myHistory.TotalCostAmount,
               AmountPaid=myHistory.AmountPaid,
               Balance=myHistory.Balance,
               KeyDate=myHistory.KeyDate}).Take(10).ToList();
        }


        //public IEnumerable<IGrouping<TransactionHistory, Transaction>> GetTransactionHistory(string phonenumber)
        //{

        //    return context.Transactions.Where(c => c.CustomerPhone == phonenumber).Include("Customer")
        //       .GroupBy(myHistory => new TransactionHistory
        //       {
        //           TransactionNo = myHistory.TransactionNo,
        //           CustomerTag = myHistory.CustomerTag,
        //           CustomerPhone = myHistory.CustomerPhone,
        //           TotalCostAmount = myHistory.TotalCostAmount,
        //           AmountPaid = myHistory.AmountPaid,
        //           Balance = myHistory.Balance,
        //           KeyDate = myHistory.KeyDate
        //       }.se
        //       }).Take(10).Select(
        //       {
        //       }).ToList();

        //}



   // public IEnumerable<IGrouping<object, Transaction>> GetTransactionHistoryByPhoneNumber(string phonenumber)
    public IEnumerable<TransactionHistory> GetTransactionHistory(string phonenumber)
    {
        //IEnumerable<IGrouping<object, Transaction>> 
            var    search = from p in context.Transactions
            where p.CustomerPhone.Equals(phonenumber)
            group p by new { p.TransactionNo, p.CustomerTag, p.AmountPaid,
            p.Balance, p.TotalCostAmount, p.KeyDate, p.CustomerPhone } into g
            select new TransactionHistory
            {
                TransactionNo=g.Key.TransactionNo,
                CustomerTag = g.Key.CustomerTag,
                AmountPaid = g.Key.AmountPaid,
                Balance = g.Key.Balance,
                TotalCostAmount = g.Key.TotalCostAmount,
                CustomerPhone= g.Key.CustomerPhone,
                KeyDate = g.Key.KeyDate

            };
        var result = search.Take(10).OrderByDescending(c=>c.KeyDate).ToList();

        return result;
    }

    //public IEnumerable<IGrouping<object, Transaction>> GetTransactionHistoryByPhoneNumber(string phonenumber)
    ////public Transaction GetTransactionHistoryByPhoneNumber(string phonenumber)
    //{
    //    IEnumerable<IGrouping<object, Transaction>> search = context.Transactions.Where(c => c.CustomerPhone == phonenumber).Include("Customer").GroupBy(c=>c.TransactionNo, c => c.KeyDate).ToList();

    //    CustomerTag = x.CustomerTag,
    //        AmountPaid = x.AmountPaid,
    //        Balance = x.Balance,
    //        TotalCostAmount = x.TotalCostAmount,
    //        CustomerPhone = x.CustomerPhone,
    //        KeyDate = x.KeyDate
    //    })
    //                                                         where p.CustomerPhone.Equals(phonenumber)
    //                                                         group p by new { p.TransactionNo, p.CustomerTag, p.AmountPaid, p.Balance, p.TotalCostAmount, p.KeyDate, p.CustomerPhone } into g

    //                                                         select new
    //                                                         {
    //                                                             CustomerTag = g.Key.CustomerTag,
    //                                                             AmountPaid = g.Key.AmountPaid,
    //                                                             Balance = g.Key.Balance,
    //                                                             g.Key.TotalCostAmount,
    //                                                             g.Key.CustomerPhone,
    //                                                             g.Key.KeyDate

    //                                                         };
    //    var result = search.ToList();

    //    return result;
    //}
}
}
