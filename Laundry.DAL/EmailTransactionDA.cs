﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.Model;
using System.Data.Entity;

namespace Laundry.DAL
{
  public  class EmailTransactionDA
    {
        public LaundryContext context = new LaundryContext();
        private string message;
        public IEnumerable<EmailTransaction> ListAll()
        {
            return context.EmailTransactions.ToList();
        }

        public IEnumerable<EmailTransaction> GetEmailTransactionByStatus(string status)
        {
            return context.EmailTransactions.Where(c=>c.MessageStatus==status).ToList();
        }

        public IEnumerable<EmailTransaction> GetEmailTransactionByTransNo(string transno)
        {
            return context.EmailTransactions.Where(c => c.TransactionNo == transno).ToList();
        }
        public EmailTransaction GetById(string id)
        {
            return context.EmailTransactions.Where(c => c.MessageID == id).FirstOrDefault();
        }
        public void Insert(EmailTransaction EmailTransactionObj)
        {
            context.EmailTransactions.Add(EmailTransactionObj);
            context.SaveChanges();
        }

        public void Update(EmailTransaction EmailTransactionObj)
        {
            context.Entry(EmailTransactionObj).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(string id)
        {
            var search = context.EmailTransactions.Where(c => c.MessageID == id).FirstOrDefault();
            context.EmailTransactions.Remove(search);
            context.SaveChanges();
        }
    }
}
