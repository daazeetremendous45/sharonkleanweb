﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.Model;
using System.Data.Entity;

namespace Laundry.DAL
{
    public class BranchDA
    {
        public LaundryContext context = new LaundryContext();
        private string message;
        public IEnumerable<Branch> ListAll()
        {
            return context.Branch.Include("CompanyDetail").ToList();
        }
        public Branch GetById(int id)
        {
            return context.Branch.Where(c => c.BranchID == id).FirstOrDefault();
        }

        public Branch GetByName(string name)
        {
            return context.Branch.Where(c => c.BranchName == name).FirstOrDefault();
        }

        public void Insert(Branch BranchDAObj)
        {
            context.Branch.Add(BranchDAObj);
            context.SaveChanges();
        }

        public void Update(Branch BranchDAObj)
        {
            context.Entry(BranchDAObj).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var search = context.Branch.Where(c => c.BranchID == id).FirstOrDefault();
            context.Branch.Remove(search);
            context.SaveChanges();
        }

    }
}
