﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Laundry.Model;
using Laundry.Model.Custom;
namespace Laundry.DAL
{
    public class PriceDetailDA
    {
        public LaundryContext context = new LaundryContext();
        public IEnumerable<PriceDetailDescriptions> ListAll()
        {
            //return context.Clothings.ToList();
            var query = from p in context.PriceDetails.ToList()
                        join g in context.Garments on p.GarmentID equals g.GarmentID
                        join cc in context.Code.Where(m => m.Codes_Type == "CleaningCategory") on p.CleaningCategory equals cc.Codes_Val
                        join cs in context.Code.Where(m => m.Codes_Type == "ServiceType") on p.ServiceType equals cs.Codes_Val 
                    into ServiceTypeInfo

                        from res in ServiceTypeInfo.DefaultIfEmpty()
                        select new PriceDetailDescriptions
                        {
                            PriceDetailID = p.PriceDetailID,
                            GarmentID = p.GarmentID,
                            GarmentName = g.GarmentName,
                            Amount = p.Amount,
                            CleaningCategory = p.CleaningCategory,
                            CleaningCategoryDescription = cc.Codes_Desc,
                            ServiceType = p.ServiceType,
                            ServiceTypeDescription = res?.Codes_Desc ,
                            UserId = p.UserId,
                            Keydate = p.Keydate
                        };
            var result = query.ToList();
            return result;

        }

        public PriceDetail GetById(int id)
        {
            return context.PriceDetails.Where(c => c.PriceDetailID == id).FirstOrDefault();
        }

        public void Insert(PriceDetail priceDetailObj)
        {
            context.PriceDetails.Add(priceDetailObj);
            context.SaveChanges();
        }

        public void Update(PriceDetail priceDetailObj)
        {
            context.Entry(priceDetailObj).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var search = context.PriceDetails.Where(c => c.PriceDetailID == id).FirstOrDefault();
            context.PriceDetails.Remove(search);
            context.SaveChanges();
        }


        public  PriceDetail GetPriceByGarmentCleaningCatServiceType(int garmentId, string cleaningCategory, string serviceType)
        {
            return context.PriceDetails.Where(c => c.GarmentID == garmentId && c.CleaningCategory == cleaningCategory &&
                                                    c.ServiceType == serviceType).FirstOrDefault();
        }

        public PriceDetail GetPriceByGarmentCleaningCat(int garmentId, string cleaningCategory)
        {
            return context.PriceDetails.Where(c => c.GarmentID == garmentId && c.CleaningCategory == cleaningCategory
                                                   ).FirstOrDefault();
        }
    }
}
