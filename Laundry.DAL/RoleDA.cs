﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.Model;

namespace Laundry.DAL
{
    public class RoleDA
    {
        public LaundryContext context = new LaundryContext();
        public IEnumerable<Role> GetAllRoles()
        {
            return context.Roles.ToList();
        }

        public Role GetRoleIdByName(string roleName)
        {
            return context.Roles.Where(c=>c.RoleName==roleName).FirstOrDefault();
        }
    }
}
