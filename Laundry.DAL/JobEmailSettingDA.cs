﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.Model;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
namespace Laundry.DAL
{
    public class JobEmailSettingDA
    {

        static SqlDataAdapter da = new SqlDataAdapter();
        static DataSet ds = new DataSet();
        public static DataSet GetEmailSetting()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = JobConnectionString.GetConnection();
                SqlCommand comm = new SqlCommand();
                comm.CommandText = "Select * FROM EmailSettings";
                comm.CommandType = CommandType.Text;
                comm.Connection = conn;
                conn.Open();
                da.SelectCommand = comm;
                da.Fill(ds);
                conn.Close();
            }
            return ds;
        }

    }
}
