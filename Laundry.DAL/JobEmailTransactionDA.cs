﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laundry.Model;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;

namespace Laundry.DAL
{
  public  class JobEmailTransactionDA
    {
      static  SqlDataAdapter da = new SqlDataAdapter();
      static DataSet ds = new DataSet();
        public static DataSet GetFailedEmailDueToInternet()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = JobConnectionString.GetConnection();
                SqlCommand comm = new SqlCommand();
                comm.CommandText = "Select * FROM EmailTransactions WHERE MessageStatus=@Status";
                comm.Parameters.AddWithValue("@Status", "Internet connection not available");
                comm.CommandType = CommandType.Text;
                comm.Connection = conn;
                conn.Open();
                da.SelectCommand = comm;
                da.Fill(ds);
                conn.Close();
            }
                return ds;
        }

        public static void UpdateFailedEmailStatus(string id)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = JobConnectionString.GetConnection();
                SqlCommand comm = new SqlCommand();
                comm.CommandText = "Update EmailTransactions SET MessageStatus='Delivered' WHERE MessageID=@ID";
                comm.Parameters.AddWithValue("@ID", id);
                comm.CommandType = CommandType.Text;
                comm.Connection = conn;
                conn.Open();
                comm.ExecuteNonQuery();
                conn.Close();
            }
        }

    
    }
}
