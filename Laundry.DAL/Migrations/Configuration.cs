namespace Laundry.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Laundry.Model;

    internal sealed class Configuration : DbMigrationsConfiguration<Laundry.DAL.LaundryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Laundry.DAL.Migrations.Configuration";
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Laundry.DAL.LaundryContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );

            CompanyDetail CompanyDetailObj = new CompanyDetail();
            CompanyDetailObj.Company_Code = "Test";
            CompanyDetailObj.Company_Name = "Test Laundry";
            CompanyDetailObj.Company_ShortName = "Test Laundry";
            CompanyDetailObj.Company_Address = "Lagos";
            CompanyDetailObj.Company_Phone1 = "07053900429";
            CompanyDetailObj.Company_Phone2 = "07053900429";
            CompanyDetailObj.Company_Email = "test@yahoo.com";
            CompanyDetailObj.Company_Username = "admin";
            CompanyDetailObj.Company_Password = "admin";
            CompanyDetailObj.Company_Flag = "A";
            CompanyDetailObj.Company_KeyDate = DateTime.Now;
            context.CompanyDetails.AddOrUpdate(c => c.Company_Username, CompanyDetailObj);


            //Roles
            context.Roles.AddOrUpdate(
           p => p.RoleName,
           new Role { RoleName = "Administrator", Status = 1, CreatedBy = "System", CreatedOn = DateTime.Today, ModifiedBy = "System", ModifiedOn = DateTime.Today },
           new Role { RoleName = "User", Status = 1, CreatedBy = "System", CreatedOn = DateTime.Today, ModifiedBy = "System", ModifiedOn = DateTime.Today }
           );

            context.LaundryMans.AddOrUpdate(p => p.Username,
             new LaundryMan
             {
                 Surname = "Administrator",
                 Othername = "Administrator",
                 Sex = "Male",
                 PhoneNumber = "07053900429",
                 Address = "TBA",
                 Username = "admin",
                 Password = Encrypt(HashPassword("admin")),
                 RoleID = 1,
                 Status = 1,
                 Flag = "A",
                 Keydate = DateTime.Now
             },
             new LaundryMan
             {
                 Surname = "Tester",
                 Othername = "Tester",
                 Sex = "Male",
                 PhoneNumber = "07087654321",
                 Address = "TBA",
                 Username = "test",
                 Password = Encrypt(HashPassword("test")),
                 RoleID = 2,
                 Status = 1,
                 Flag = "C",
                 Keydate = DateTime.Now
             }
             );


            /* EmailSetting EmailSettingObj = new EmailSetting();
             EmailSettingObj.SmtpServer = "mail.sharonklean.com";
             EmailSettingObj.PortNo = 25;
             EmailSettingObj.FromEmail = "test@sharonklean.com";
             EmailSettingObj.Password = "yakdseeker@1";
             EmailSettingObj.UserId = "System";
             EmailSettingObj.Flag = "A";
             EmailSettingObj.KeyDate = DateTime.Now;
             context.EmailSettings.AddOrUpdate(c => c.UserId, EmailSettingObj);
             */
            //Starch Level

            context.Code.AddOrUpdate(
              p => p.Codes_Desc,
              new Codes { Codes_Type = "StarchLevel", Codes_Desc = "Light", Codes_Val = "Li", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
              new Codes { Codes_Type = "StarchLevel", Codes_Desc = "Medium", Codes_Val = "Me", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
              new Codes { Codes_Type = "StarchLevel", Codes_Desc = "Heavy", Codes_Val = "He", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" }
              );


            //Pressing Options

            context.Code.AddOrUpdate(
              p => p.Codes_Desc,
              new Codes { Codes_Type = "PressingOption", Codes_Desc = "Dry", Codes_Val = "D", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
              new Codes { Codes_Type = "PressingOption", Codes_Desc = "Steam", Codes_Val = "S", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" }
              );

            //Collection Type

            context.Code.AddOrUpdate(
              p => p.Codes_Desc,
              new Codes { Codes_Type = "CollectionType", Codes_Desc = "Folding", Codes_Val = "F", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
              new Codes { Codes_Type = "CollectionType", Codes_Desc = "Hanging", Codes_Val = "H", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
               new Codes { Codes_Type = "CollectionType", Codes_Desc = "Crisp", Codes_Val = "C", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
              new Codes { Codes_Type = "CollectionType", Codes_Desc = "No Crisp", Codes_Val = "N", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" }
              );

            //Cleaning Category
            context.Code.AddOrUpdate(
            p => p.Codes_Desc,
            new Codes { Codes_Type = "CleaningCategory", Codes_Desc = "Men", Codes_Val = "1", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
            new Codes { Codes_Type = "CleaningCategory", Codes_Desc = "Ladies", Codes_Val = "2", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
             new Codes { Codes_Type = "CleaningCategory", Codes_Desc = "HouseHold", Codes_Val = "3", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" }
            );

            //ServiceType
            context.Code.AddOrUpdate(
            p => p.Codes_Desc,
            new Codes { Codes_Type = "ServiceType", Codes_Desc = "Laundry", Codes_Val = "1", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
            new Codes { Codes_Type = "ServiceType", Codes_Desc = "Dry-Cleaning", Codes_Val = "2", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
             new Codes { Codes_Type = "ServiceType", Codes_Desc = "Press Only", Codes_Val = "3", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" }
           );

            //Color
            context.Code.AddOrUpdate(
            p => p.Codes_Desc,
            new Codes { Codes_Type = "Color", Codes_Desc = "Black", Codes_Val = "1", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
            new Codes { Codes_Type = "Color", Codes_Desc = "Brown", Codes_Val = "2", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
            new Codes { Codes_Type = "Color", Codes_Desc = "White", Codes_Val = "3", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
            new Codes { Codes_Type = "Color", Codes_Desc = "Grey", Codes_Val = "4", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
            new Codes { Codes_Type = "Color", Codes_Desc = "Blue", Codes_Val = "5", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
            new Codes { Codes_Type = "Color", Codes_Desc = "Yellow", Codes_Val = "6", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
            new Codes { Codes_Type = "Color", Codes_Desc = "Ash", Codes_Val = "7", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
            new Codes { Codes_Type = "Color", Codes_Desc = "Purple", Codes_Val = "8", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" }
            );

            //Branch

            // CompanyDetailDA companyDetailDA = new CompanyDetailDA();

            // int CompanyID = companyDetailDA.GetCompanyDetail().Company_Id;
            // context.Branch.AddOrUpdate(
            //p => new { p.BranchName, p.Company_Id },
            //new Branch { BranchName = "Head Office", Address = "4, Ayinde Sanni Oregun, Ikeja, Lagos.", PhoneNumber1 = "08060001121", PhoneNumber2 = "07018122842", Company_Id = CompanyID, Status = 1, CreatedBy = "System", CreatedOn = DateTime.Today, ModifiedBy = "System", ModifiedOn = DateTime.Today },
            //new Branch { BranchName = "Branch 1", Address = "Lagos State.", PhoneNumber1 = "07017991799", Company_Id = CompanyID, Status = 1, CreatedBy = "System", CreatedOn = DateTime.Today, ModifiedBy = "System", ModifiedOn = DateTime.Today },
            //new Branch { BranchName = "Branch 2", Address = "Ibadan State.", PhoneNumber1 = "07018122842", PhoneNumber2 = "08037213042", Company_Id = CompanyID, Status = 1, CreatedBy = "System", CreatedOn = DateTime.Today, ModifiedBy = "System", ModifiedOn = DateTime.Today }

            //);

            //DeliveryOption
            context.Code.AddOrUpdate(
            p => p.Codes_Desc,
            new Codes { Codes_Type = "DeliveryOption", Codes_Desc = "Self", Codes_Val = "1", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" },
            new Codes { Codes_Type = "DeliveryOption", Codes_Desc = "Home Delivery", Codes_Val = "2", Codes_Flag = "A", Codes_KeyDate = DateTime.Today, Codes_UserId = "System" }
           );

        }

        public static string result;
        public static string HashPassword(string password)
        {
            System.Security.Cryptography.SHA1Managed sha1 = new System.Security.Cryptography.SHA1Managed();
            byte[] hash = sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            System.Text.StringBuilder sb = new System.Text.StringBuilder(hash.Length * 2);
            foreach (byte b in hash)
            {
                sb.Append(b.ToString("x2"));
            }
            result = sb.ToString();
            return result;
        }

        public static string Encrypt(string password)
        {
            int len = password.Length;
            string newpassword = "";
            char firstchar = password[0];
            char lastchar = password[password.Length - 1];
            string last_first = $"{lastchar}{firstchar}";
            int insert_position = len / 2;
            System.Text.StringBuilder sbd = new System.Text.StringBuilder(password);
            sbd[0] = lastchar;
            sbd[len - 1] = firstchar;
            newpassword = sbd.ToString();
            result = newpassword.Insert(insert_position, last_first);
            return result;
        }

    }
}
