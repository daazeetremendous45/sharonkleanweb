﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Laundry.Model;
using Laundry.Model.Custom;

namespace Laundry.DAL
{
    public class ClothingDA
    {
        public LaundryContext context = new LaundryContext();
        public IEnumerable<ClothingDetail> ListAll()
        {
            //return context.Clothings.ToList();
            var query = from c in context.Clothings.ToList()
                        join g in context.Code.Where(m => m.Codes_Type == "GenderType") on c.GenderType equals g.Codes_Val
                        join s in context.Code.Where(m => m.Codes_Type == "ServiceType") on c.ServiceType equals s.Codes_Val

                        select new ClothingDetail
                        {
                            //Sn = c.Sn,
                            ClothingID = c.ClothingID,
                            ClothDesc = c.ClothDesc,
                            Amount = c.Amount,
                            GenderType = c.GenderType,
                            ServiceType = c.ServiceType,
                            GenderTypeDescription = g.Codes_Desc,
                            ServiceTypeDescription = s.Codes_Desc,
                        };
            var result = query.ToList();
            return result;

        }

        public Clothing GetById(int id)
        {
            return context.Clothings.Where(c => c.ClothingID == id).FirstOrDefault();
        }

        //public Clothing GetByClothValue(string cloth_val)
        //{
        //    //Get Code Description By Code Value
        //    return context.Clothings.Where(c => c.ClothId == cloth_val).FirstOrDefault();
        //}

        public Clothing GetByClothDesc(string cloth_desc)
        {
            //Get Code Description By Code Value
            return context.Clothings.Where(c => c.ClothDesc == cloth_desc).FirstOrDefault();
        }
        public void Insert(Clothing ClothingDAObj)
        {
            context.Clothings.Add(ClothingDAObj);
            context.SaveChanges();
        }

        public void Update(Clothing ClothingDAObj)
        {
            context.Entry(ClothingDAObj).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var search = context.Clothings.Where(c => c.ClothingID == id).FirstOrDefault();
            context.Clothings.Remove(search);
            context.SaveChanges();
        }

        ////Get the description of existing 
        //public Clothing ClothDescription(string cloth_id, string cloth_desc)
        //{
        //    //Get Code Description By Code Value
        //    return context.Clothings.Where(c => c.ClothId != cloth_id && c.ClothDesc == cloth_desc).FirstOrDefault();
        //}

        public async  Task<Clothing> GetByGarmentGenderServiceTypeAsync(string clothDesc, string gender, string serviceType)
        {
            return await context.Clothings.Where(c => c.ClothDesc == clothDesc && c.GenderType == gender && c.ServiceType == serviceType).FirstOrDefaultAsync();
        }
    }
}
