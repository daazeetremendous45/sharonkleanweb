﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
namespace Laundry.DAL
{
    class JobConnectionString
    {
        public static string GetConnection()
        {
            string connString = ConfigurationManager.ConnectionStrings["LaundryContext"].ConnectionString;
            return connString;
        }
    }
}
