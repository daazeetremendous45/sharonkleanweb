﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Laundry.Model;

namespace Laundry.DAL
{
    public class GarmentDA
    {
        public LaundryContext context = new LaundryContext();
        public IEnumerable<Garment> ListAll()
        {
           return context.Garments.ToList();
        }

        public Garment GetById(int id)
        {
            return context.Garments.Where(c => c.GarmentID == id).FirstOrDefault();
        }

        public Garment GetByName(string name)
        {
            return context.Garments.Where(c => c.GarmentName == name).FirstOrDefault();
        }

        public void Insert(Garment garmentObj)
        {
            context.Garments.Add(garmentObj);
            context.SaveChanges();
        }

        public void Update(Garment garmentObj)
        {
            context.Entry(garmentObj).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            var search = context.Garments.Where(c => c.GarmentID == id).FirstOrDefault();
            context.Garments.Remove(search);
            context.SaveChanges();
        }
    }
}
