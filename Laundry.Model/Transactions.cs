﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Laundry.Model
{
    public class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TransactionId { get; set; }

        [Display(Name = "Transaction Number")]
        public string TransactionNo { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "Tag")]
        public string CustomerTag { get; set; }

        [Display(Name = "Phone No")]
        public string CustomerPhone { get; set; }

        [EmailAddress]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Display(Name = "Garment Type")]
        public string ClothCode { get; set; }

        public string CleaningCategory { get; set; }

        public string ServiceType { get; set; }

        public string DeliveryOption { get; set; }

        [Display(Name = "Rate")]
        public double UnitPrice { get; set; }

        public int Quantity { get; set; }

        public double Amount { get; set; }

        [Display(Name = "Laundry Type")]
        public string LaundryType { get; set; }

        public string Colour { get; set; }

        public string Address { get; set; }

        [Display(Name = "Payment Mode")]
        public string PaymentMode { get; set; }

        [Display(Name = "Amount Paid")]
        public double AmountPaid { get; set; }

        public double Balance { get; set; }


        [Display(Name = "Total Cost")]
        public double TotalCostAmount { get; set; }

        public double ExPressAmount { get; set; }

        public string ClothStatus { get; set; }
        public string UserId { get; set; }

        public string Flag { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime KeyDate { get; set; }

        public string HeaderDetail { get; set; }//H or D. The first entry is H while subsequent entry is D.

        [Display(Name = "Collection Date")]
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CollectionDate { get; set; }

        [Display(Name = "Collection Type")]

        public string CollectionType { get; set; }

        [Display(Name = "Starch Level")]
        public string StarchLevel { get; set; }

        [Display(Name = "Brand Name")]
        public string BrandName { get; set; }

        [Display(Name = "Pressing Option")]
        public string PressingOption { get; set; }

        [Display(Name = "Additional Laundry Info")]
        public string MoreInfo { get; set; }

        public int BranchID { get; set; }

        public int Company_Id { get; set; }

        [ForeignKey("Customer")]
        public string CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

    }
}
