﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laundry.Model
{
  public class Branch
    {
        [Key]
        public int BranchID { get; set; }
        [Display(Name ="Branch Name")]
        public string BranchName { get; set; }
        
        public string Address { get; set; }

        [Display(Name = "Phone Number 1")]
        public string PhoneNumber1 { get; set; }

        [Display(Name = "Phone Number 2")]
        public string PhoneNumber2 { get; set; }

        [ForeignKey("CompanyDetail")]
        public int Company_Id { get; set; }

        public int Status { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public virtual CompanyDetail CompanyDetail { get; set; }
    }
}
