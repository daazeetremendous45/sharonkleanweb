﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Laundry.Model
{
    public class TransactionHistory
    {
        
        [Display(Name = "Transaction Number")]
        public string TransactionNo { get; set; }

        [Display(Name = "Tag")]
        public string CustomerTag { get; set; }

        [Display(Name = "Phone No")]
        public string CustomerPhone { get; set; }

       
        [Display(Name = "Amount Paid")]
        public double AmountPaid { get; set; }

        public double Balance { get; set; }


        [Display(Name = "Total Cost")]
        public double TotalCostAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime KeyDate { get; set; }

    }
}
