﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Laundry.Model
{
   public class EmailTransaction
    {
        [Key]
        public string  MessageID { get; set; }
        
        public string TransactionNo { get; set; }

        [EmailAddress]
        public string EmailAddress { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string EmailSignature { get; set; }
        public string MessageStatus { get; set; }

        public DateTime KeyDate { get; set; }

        public string UserId { get; set; }
    }
}
