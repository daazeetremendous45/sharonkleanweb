﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Laundry.Model
{
    public class Garment
    {
        [DisplayName("Garment ID")]
        [Key]
        public int GarmentID { get; set; }

        [DisplayName("Garment Name")]
        public string GarmentName { get; set; }
        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
