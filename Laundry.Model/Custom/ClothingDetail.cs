﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Laundry.Model.Custom
{
    public class ClothingDetail
    {
        //public int Sn { get; set; }

        [DisplayName("Cloth Code")]
        public int ClothingID { get; set; }

        [DisplayName("Cloth Description")]
        public string ClothDesc { get; set; }

        [Required]
        public double Amount { get; set; }

        [DisplayName("Gender")]
        public string GenderType { get; set; }

        [DisplayName("Service Type")]
        public string ServiceType { get; set; }

        [DisplayName("Gender")]
        public string GenderTypeDescription { get; set; }

        [DisplayName("Service Type")]
        public string ServiceTypeDescription { get; set; }
        public string UserId { get; set; }
        public string Flag { get; set; }
        public DateTime Keydate { get; set; }
    }
}
