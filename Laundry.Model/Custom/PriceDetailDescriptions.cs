﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Laundry.Model.Custom
{
    public class PriceDetailDescriptions
    {
        public int PriceDetailID { get; set; }
        public int GarmentID { get; set; }

        [DisplayName("Garment Name")]
        public string GarmentName { get; set; }
        public double Amount { get; set; }
        [DisplayName("Cleaning Category")]
        public string CleaningCategory { get; set; }
        [DisplayName("Cleaning Category")]
        public string CleaningCategoryDescription { get; set; }
        [DisplayName("Service Type")]
        public string ServiceType { get; set; }
        [DisplayName("Service Type")]
        public string ServiceTypeDescription { get; set; }
        public string UserId { get; set; }
        public string Flag { get; set; }
        public DateTime Keydate { get; set; }
    }
}
