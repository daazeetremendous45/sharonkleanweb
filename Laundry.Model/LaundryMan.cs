﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Laundry.Model
{
    public class LaundryMan
    {
        [Key]
        public int LaundryManID { get; set; }
        public string Surname { get; set; }
        public string Othername { get; set; }

        public string Sex { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public int Status { get; set; }
        public int Company_Id { get; set; }
        
        public int? BranchID { get; set; }

        [Display(Name = "Role")]
        [ForeignKey("Role")]
        public int RoleID { get; set; }
        public string Flag { get; set; }
        public DateTime Keydate { get; set; }
        public virtual Role Role { get; set; }
        
        public virtual Branch Branch { get; set; }
    }
}
